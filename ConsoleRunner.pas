unit ConsoleRunner;

interface
uses
  Windows;

(******************************************************************************
  ������������� ������ ����������� ����������.
  !! ����� !! ������������� ��� ������� �� ����������(!) ����������.
  !! ����� !! �� ������������ ����������, �������� ����������� ���������������
  �����/������ (������ �������: "timeout /t 20"). � ��� ����� � �������
  �������� ������.

  ����������� ����� ����� ������������� �������� ������������� � ��������
  �������.

  ����������� ����� ������ � ������ ��������� ���������� ������������� �
  ��������������� ������ ������������� ��������.

  ���� � ���������� ����������� (� ����� ����� ��������) �������������
  � ����������� ����� ����� ��������� ��������.

  @param  CommandLine ��������� ������ (� ��� ����� ���������) ��� �������
    ��������� ����������.
 ******************************************************************************)
procedure ExecConsoleAppInteractively(CommandLine: AnsiString); overload;

(******************************************************************************
  ������������� ������ ����������� ����������
  ����� �������������� ������������ ����� ��� ������ � �������� ���������
  ��������.
  !! ����� !! ������������� ��� ������� �� ����������(!) ����������.
  !! ����� !! �� ������������ ����������, �������� ����������� ���������������
  �����/������ (������ �������: "timeout /t 20"). � ��� ����� � �������
  �������� ������.

  ������ �� ����� ������� ������� �� ������ �� � ���������� ������ ��
  �� ��������

  @param  CommandLine: ��������� ������ (� ��� ����� ���������) ��� �������
    ��������� ��������

  @param  StdIn, StdOut, StdErr: ������ ������ (� �.�. ������), ��������,
    ��������������, �� ������, ������, ������
 ******************************************************************************)
procedure ExecConsoleAppInteractively(
  CommandLine: AnsiString;
  StdIn, StdOut, StdErr: THandle
); overload;

implementation
uses
  SysUtils,
  Classes;

var

  hConsoleInput: THandle;

function KeyPressed: Boolean;
var
  NumberOfEvents: DWORD;
  NumRead: DWORD;
  InputRec: TInputRecord;
  Pressed: boolean;
begin
  Pressed := False;
  GetNumberOfConsoleInputEvents(hConsoleInput, NumberOfEvents);
  if NumberOfEvents > 0 then
  begin
    if PeekConsoleInput(hConsoleInput, InputRec, 1,NumRead) then
    begin
      if (InputRec.EventType = KEY_EVENT) and
        (InputRec.Event.KeyEvent.bKeyDown) then
      begin
        Pressed := True;
      end
      else
      begin
        ReadConsoleInput(hConsoleInput, InputRec, 1,NumRead);
      end;
    end;
  end;
  Result := Pressed;
end;

function ReadKey: char;
var
  NumRead: DWORD;
  InputRec: TInputRecord;
begin
  repeat
  until KeyPressed;
  ReadConsoleInput(hConsoleInput, InputRec, 1,NumRead);

  Result := InputRec.Event.KeyEvent.AsciiChar;
end;

procedure ForwardPipe(source, dest: THandle);
var
  szBuffer: array[0..256] of Char;
  dwNumberOfBytesRead: DWORD;
  dwNumberOfBytesWritten: DWORD;
  dwTotalBytes: DWORD;
  bTest: Boolean;
begin
  if PeekNamedPipe(source, nil, 0, nil, @dwTotalBytes, nil)
  and (dwTotalBytes > 0) then
  begin
    bTest := ReadFile(source, szBuffer, 256, dwNumberOfBytesRead,
      nil);
    if bTest and (dwNumberOfBytesRead > 0) then
      WriteFile(dest, szBuffer, dwNumberOfBytesRead, dwNumberOfBytesWritten, nil);
  end;
end;

procedure ForwardConsoleInputToPipe(Pipe: THandle);
var
  ch:char;
  dwNumberOfBytesWritten: DWORD;
begin
  while keypressed do
  begin
    ch := readkey;
    WriteFile(Pipe, ch, sizeof(ch), dwNumberOfBytesWritten, nil);
    if ch = #13 then
    begin // ���������� �� ����� ������ windows-style (����� ������ ����� ������������� ���� �� �����)
      ch := #10;
      WriteFile(Pipe, ch, sizeof(ch), dwNumberOfBytesWritten, nil);
    end;
  end;
end;

procedure ExecConsoleAppInteractively(CommandLine: AnsiString; StdIn, StdOut, StdErr: THandle);
var
  sa: TSECURITYATTRIBUTES;
  si: TSTARTUPINFO;
  pi: TPROCESSINFORMATION;
  hPipeInputRead: THANDLE;
  hPipeInputWrite: THANDLE;
  hPipeOutputRead: THANDLE;
  hPipeOutputWrite: THANDLE;
  hPipeErrorsRead: THANDLE;
  hPipeErrorsWrite: THANDLE;
  Res: Boolean;

begin
  sa.nLength := sizeof(sa);
  sa.bInheritHandle := true;
  sa.lpSecurityDescriptor := nil;
  CreatePipe(
    hPipeInputRead,   // ���������� � ��������� ��������
    hPipeInputWrite,  // ���� ������
    @sa, 0);
  CreatePipe(
    hPipeOutputRead,  // ������ ������
    hPipeOutputWrite, // ���������� � ��������� ��������
    @sa, 0);
  CreatePipe(
    hPipeErrorsRead,  // ������ ������
    hPipeErrorsWrite, // ���������� � ��������� ��������
    @sa, 0);
  try
    ZeroMemory(@si, SizeOf(si));
    ZeroMemory(@pi, SizeOf(pi));
    si.cb := SizeOf(si);
    si.dwFlags := STARTF_USESHOWWINDOW or STARTF_USESTDHANDLES;
    si.wShowWindow := SW_HIDE;
    si.hStdInput := hPipeInputRead;
    si.hStdOutput := hPipeOutputWrite;
    si.hStdError := hPipeErrorsWrite;

    (* Remember that if you want to execute an app with no parameters you nil the
       second parameter and use the first, you can also leave it as is with no
       problems.                                                                 *)

    Res := CreateProcess(nil, pchar(CommandLine), nil, nil, true,
      CREATE_NEW_CONSOLE or NORMAL_PRIORITY_CLASS, nil, nil, si, pi);

    // Procedure will exit if CreateProcess fail
    if not Res then
      Exit;

    try
      //Process pipes
      repeat
        ForwardPipe(StdIn, hPipeInputWrite);
        ForwardPipe(hPipeOutputRead, StdOut);
        ForwardPipe(hPipeErrorsRead, StdErr);
        ForwardConsoleInputToPipe(hPipeInputWrite);
      until WaitForSingleObject(pi.hProcess, 0) = WAIT_OBJECT_0;
      // ���������� ������� ������� ������ �� ����� ���������� ��������
      ForwardPipe(hPipeOutputRead, StdOut);
      ForwardPipe(hPipeErrorsRead, StdErr);

    finally
      CloseHandle(pi.hProcess);
    end;

  finally
    CloseHandle(hPipeInputRead);
    CloseHandle(hPipeInputWrite);
    CloseHandle(hPipeOutputRead);
    CloseHandle(hPipeOutputWrite);
    CloseHandle(hPipeErrorsRead);
    CloseHandle(hPipeErrorsWrite);
  end;
end;

procedure ExecConsoleAppInteractively(CommandLine: AnsiString); overload;
begin
    ExecConsoleAppInteractively(CommandLine, GetStdHandle(STD_INPUT_HANDLE), GetStdHandle(STD_OUTPUT_HANDLE), GetStdHandle(STD_ERROR_HANDLE));
end;

initialization
  hConsoleInput := GetStdHandle(STD_INPUT_HANDLE);
end.
