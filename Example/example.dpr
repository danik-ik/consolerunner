program Example;

{$APPTYPE CONSOLE}

uses
  ConsoleRunner in '..\ConsoleRunner.pas';

var
  childApp: string;
begin
  if ParamCount = 0 then
  begin
    Writeln('Use command to run in child process as parameter');
    exit;
  end;
  childApp := ParamStr(1);
  ExecConsoleAppInteractively(childApp);
end.
